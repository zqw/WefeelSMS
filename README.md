短信接口动态链接库WefeelSMS.dll
2013.12.26更新为2.2.0.0版本。修改了长短信分割为2条以上短信时分割错误。

v2.1版
增加了自动分段发送超长短信功能，不再受70个字限制。遇到超长短信dll会自动处理，无需干预判断。短信长度程序上没有限制，可能会受运营商限制。

v1.0版
Delphi7.0，需安装Turbo Power的Async Professional，用到其中ApdComPort控件

特点：
1、发送接收短信
2、可监控AT命令
3、可查询发送队列长度
4、最小连续发送短信间隔时间可调，确保不会漏收短信。
5、串口可设置。
6、短信中心号码从SIM卡读出
7、收到的短信不在SIM卡或手机保存。