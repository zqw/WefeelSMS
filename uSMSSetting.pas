unit uSMSSetting;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons;

type
  TfrmSMSSetting = class(TForm)
    Label1: TLabel;
    ComPort: TComboBox;
    Label2: TLabel;
    ComBaud: TComboBox;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSMSSetting: TfrmSMSSetting;

implementation

uses SMSExports, uMain;

{$R *.dfm}

procedure TfrmSMSSetting.BitBtn1Click(Sender: TObject);
begin
  SMSStop();
  SMSSetting(StrToInt(ComPort.Text),StrToInt(ComBaud.Text));
  try
    SMSStart();
    frmMain.StatusBar.Panels[0].Text := '连接短信设备成功';
  except
    frmMain.StatusBar.Panels[0].Text := '连接短信设备失败';
  end;
  self.Hide;
end;

procedure TfrmSMSSetting.BitBtn2Click(Sender: TObject);
begin
  self.Hide;
end;

end.
