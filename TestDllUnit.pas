unit TestDllUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Timer1: TTimer;
    Memo1: TMemo;
    Button2: TButton;
    Edit1: TEdit;
    Edit2: TEdit;
    Button3: TButton;
    Label1: TLabel;
    Button4: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses WefeelSMSDLL;

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
  SMSCreate();
  SMSSetting(3,9600);
  try
    SMSStart();
  except

  end;
  Timer1.Enabled:=true;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  Timer1.Enabled:=false;
  SMSStop();
  SMSFree();
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  SMSPutSMS(PChar(Edit1.Text),PChar(Edit2.Text),1);
end;

procedure TForm1.Timer1Timer(Sender: TObject);
var
  Code,Time,Content:array[0..500] of char;//string;
  Line:string;
  pLine:array[0..500] of char;
begin
  if SMSGetLine(pLine) then
    Memo1.Lines.Add(pLine);
  if SMSGetSMS(Code,Time,Content) then
    Memo1.Lines.Add(AnsiString(Code)+'*'+AnsiString(Time)+'*'+AnsiString(Content));
  Label1.Caption:=IntToStr(SMSQueueLength());
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  SMSSettingWindow();
end;

end.
