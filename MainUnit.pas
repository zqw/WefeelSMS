unit MainUnit;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  ComObj, ActiveX, TESMS_TLB, StdVcl;

type
  TSMS = class(TAutoObject, ISMS)
  protected
    procedure sendmsg(comnumber: Integer; const phone, msg: WideString);safecall;
    //procedure Open

  end;

implementation

uses ComServ;

function SEncodeMobNO(SmobNO: string): string;
//要想发送中文短信必须使用Modem的PDU方式。这个函数是将手机号码进行PDU编码。
var
　TempPchar: Pchar;
　i: integer;
　Str: string;
begin
　if (copy(smobno, 1, 1) = ’+’) then //判断是否包含国家编码
　　SmobNO := copy(smobno, 2, length(smobno) - 1); //去掉手机号码中的’+’

　if ((length(SmobNO) mod 2) = 1) then
　　SmobNO := SmobNO + ’F’;

  TempPchar := Pchar(SmobNO); //将字符串 Char数组化

　i := 0;
　Str := ’’;
　while i < length(TempPchar) do begin
　　Str := Str + TempPchar[i + 1] + TempPchar[i];
　　i := i + 2;
　end;
　result := Str;
end;

function EncodeChinese(Input: WideString): string;//将信息内容进行PDU编码
var
　i: Integer;
begin
　Result := ’’;
　for i := 1 to Length(Input) do
　　Result := Result + Format(’%4.4X’, [ord(Input[i])]);
end;

procedure Tsms.sendmsg(comnumber: Integer; const phone, msg: WideString);
//发送短信的方法
var
　apdcomport:Tapdcomport;
　r,s,s2,s3,s4,s5:string;
　cmdlong,tmp:integer;
　msgs:WideString;
begin
　apdcomport:=TApdComPort.Create(nil);//创建串口通信对象
　apdcomport.AutoOpen:=false;//关闭自动打开属性
　apdcomport.Open:=false;
　apdcomport.ComNumber:=comnumber;//设置串行通信口
　apdcomport.Baud:=9600;//设置串口波特率
　msgs:=msg;
　s:=’0031000D9168’ ;
　//PDU编码属性，这种方法是不需要设置短信中心号码的，因为现的手机SIM卡已经写好了
　s2:=SEncodeMobNO(phone);//对手机号码进行PDU编码
　s3:=’0008A7’;
　s4:=’’;
　s5:=EnCodeChinese(msgs);
　tmp:=length(s5)div 2;
　s4:=format(’%X’,[tmp]);
　if length(s4)<2 then
　　s4:=’0’+s4;
　//计算PDU编码长度
　r:=s+s2+s3+s4+s5+^Z;
　cmdlong:=(length(r)-2) div 2;
　apdcomport.Open:=true;//打开串行口
　apdcomport.Output:=’AT+CMGF=0’#13;//设置Modem为PDU模式
　delayticks(7,true);//延时
　apdcomport.Output:=’AT+CMGS=’+inttostr(cmdlong)+#13;//设置信息长度，这里应为PDU编码长度的1/2.
　delayticks(7,true);
　apdcomport.Output:=r;//发送短信。
　delayticks(9,true);
　apdcomport.Open:=false;
　apdcomport.Free;

end;

initialization
  TAutoObjectFactory.Create(ComServer, TSMS, Class_SMS,
    ciMultiInstance, tmApartment);
end.
