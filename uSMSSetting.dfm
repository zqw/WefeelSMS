object frmSMSSetting: TfrmSMSSetting
  Left = 276
  Top = 199
  BorderStyle = bsDialog
  Caption = #30701#20449#35774#32622
  ClientHeight = 85
  ClientWidth = 310
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 24
    Top = 24
    Width = 36
    Height = 12
    Caption = #20018#21475#65306
  end
  object Label2: TLabel
    Left = 24
    Top = 56
    Width = 36
    Height = 12
    Caption = #36895#29575#65306
  end
  object ComPort: TComboBox
    Left = 72
    Top = 16
    Width = 65
    Height = 20
    ItemHeight = 12
    ItemIndex = 0
    TabOrder = 0
    Text = '1'
    Items.Strings = (
      '1'
      '2'
      '3'
      '4'
      '5'
      '6'
      '7'
      '8'
      '9'
      '10'
      '11'
      '12'
      '13'
      '14'
      '15'
      '16'
      '17'
      '18'
      '19'
      '20')
  end
  object ComBaud: TComboBox
    Left = 72
    Top = 48
    Width = 65
    Height = 20
    ItemHeight = 12
    ItemIndex = 2
    TabOrder = 1
    Text = '9600'
    Items.Strings = (
      '2400'
      '4800'
      '9600'
      '19200'
      '38400'
      '57600'
      '115200')
  end
  object BitBtn1: TBitBtn
    Left = 184
    Top = 16
    Width = 75
    Height = 25
    TabOrder = 2
    OnClick = BitBtn1Click
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 184
    Top = 48
    Width = 75
    Height = 25
    TabOrder = 3
    OnClick = BitBtn2Click
    Kind = bkCancel
  end
end
