unit TestUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, AdPort,DateUtils;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Memo1: TMemo;
    Timer1: TTimer;
    Memo2: TMemo;
    Edit1: TEdit;
    Edit2: TEdit;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses SMS, SettingUnit;
var
  sms:TShortMessage;

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
  sms:=TShortMessage.Create();
  sms.ComNumber:=1;
  sms.Baud:=9600;
  sms.Open;
  Timer1.Enabled:=true;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  Timer1.Enabled:=false;
  sms.Close;
  sms.Free;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  //sms.AddSMSToSendSpool('13807035593','test');
  sms.AddSMSToSendSpool(PChar(Edit2.Text),PChar(Edit1.Text),1);
end;

procedure TForm1.Timer1Timer(Sender: TObject);
var
  Code,Time,Content:array[0..500] of char;//string;
  Line:string;
  pLine:array[0..500] of char;
  ID,State:integer;
begin
  if sms.GetFromRecvSpool(pLine) then
    Memo2.Lines.Add(pLine);
  if sms.GetSMSFromRecvSpool(Code,Time,Content) then
    Memo1.Lines.Add(AnsiString(Code)+'*'+AnsiString(Time)+'*'+AnsiString(Content));
  if sms.GetFromResultSpool(ID,State) then
    Memo1.Lines.Add(IntToStr(ID)+'*'+IntToStr(State));
  Button5.Caption:=IntToStr(sms.WaitingSendCount);
end;

//从Project信息中取版本号并显示在标题
function GetProductVersion():string;
const
  InfoNum = 10;
  InfoStr: array[1..InfoNum] of string = ('CompanyName', 'FileDescription', 'FileVersion', 'InternalName', 'LegalCopyright', 'LegalTradeMarks', 'OriginalFileName', 'ProductName', 'ProductVersion', 'Comments');
var
  S: string;
  n, Len: DWORD;
  Buf: PChar;
  Value: PChar;
begin
  //取系统版本
  S := Application.ExeName;
  n := GetFileVersionInfoSize(PChar(S), n);
  if n > 0 then
  begin
    Buf := AllocMem(n);
    GetFileVersionInfo(PChar(S), 0, n, Buf);
    if VerQueryValue(Buf, PChar('StringFileInfo\080403A8\' + InfoStr[3]), Pointer(Value), Len) then
    begin
        Result:=Value;
    end;
    FreeMem(Buf, n);
  end;
end;

procedure TForm1.Button5Click(Sender: TObject);
begin
  ShowMessage(IntToStr(sms.WaitingSendCount));
end;

procedure TForm1.Button6Click(Sender: TObject);
var
  SettingForm: TSettingForm;
begin
  SettingForm:=TSettingForm.Create(nil);
  SettingForm.ComPort.Text:=IntToStr(sms.ComNumber);
  SettingForm.ComBaud.Text:=IntToStr(sms.Baud);
  if SettingForm.ShowModal()=mrOK then
  begin
    sms.Close;
    sms.ComNumber:=StrToInt(SettingForm.ComPort.Text);
    sms.Baud:=StrToInt(SettingForm.ComBaud.Text);
    sms.Open;
  end;
  SettingForm.Free;
end;

var
  aa:TDateTime;

procedure TForm1.Button7Click(Sender: TObject);
begin
  if aa=0 then   aa:=Now();
  //Memo2.Lines.Add(DateTimeToStr(aa)+' '+DateTimeToStr(Now())+' '+FloatToStr((Now()-aa)*10000));
  Memo2.Lines.Add(DateTimeToStr(aa)+' '+DateTimeToStr(IncSecond(aa))+' '+FloatToStr((IncSecond(aa)-aa)*10000));
end;

end.
