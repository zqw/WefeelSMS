library WefeelSMS;



uses
  ComServ,
  PDU in 'PDU.pas',
  SMS in 'SMS.pas',
  SMSExports in 'SMSExports.pas',
  SettingUnit in 'SettingUnit.pas' {SettingForm};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer,
  SMSCreate,
  SMSSetting,
  SMSStart,
  SMSStop,
  SMSFree,
  SMSPutSMS,
  SMSGetSMS,
  SMSGetLine,
  SMSQueueLength,
  SMSSettingWindow,
  SMSGetState;



{$R *.RES}


begin

end.
