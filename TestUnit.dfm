object Form1: TForm1
  Left = 258
  Top = 114
  Width = 696
  Height = 480
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 128
    Top = 64
    Width = 75
    Height = 25
    Caption = 'Open'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 128
    Top = 112
    Width = 75
    Height = 25
    Caption = 'Close'
    TabOrder = 1
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 240
    Top = 64
    Width = 75
    Height = 25
    Caption = 'Send'
    TabOrder = 2
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 240
    Top = 112
    Width = 75
    Height = 25
    Caption = 'Recv'
    TabOrder = 3
  end
  object Memo1: TMemo
    Left = 32
    Top = 296
    Width = 609
    Height = 145
    Lines.Strings = (
      'Memo1')
    TabOrder = 4
  end
  object Memo2: TMemo
    Left = 32
    Top = 152
    Width = 617
    Height = 129
    Lines.Strings = (
      'Memo2')
    TabOrder = 5
  end
  object Edit1: TEdit
    Left = 336
    Top = 80
    Width = 121
    Height = 21
    TabOrder = 6
    Text = 'Edit1'
  end
  object Edit2: TEdit
    Left = 336
    Top = 56
    Width = 121
    Height = 21
    TabOrder = 7
    Text = '13807035593'
  end
  object Button5: TButton
    Left = 504
    Top = 64
    Width = 105
    Height = 25
    Caption = 'CountWaitingSend'
    TabOrder = 8
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 120
    Top = 24
    Width = 75
    Height = 25
    Caption = 'Setting'
    TabOrder = 9
    OnClick = Button6Click
  end
  object Button7: TButton
    Left = 40
    Top = 64
    Width = 75
    Height = 25
    Caption = 'Button7'
    TabOrder = 10
    OnClick = Button7Click
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 100
    OnTimer = Timer1Timer
    Left = 400
    Top = 152
  end
end
